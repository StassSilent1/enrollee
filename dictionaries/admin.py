from django.contrib import admin
from dictionaries.models import (
  documents as dictDocuments,
  competitions as dictCompetitions
  )
from relationships.models import(
  documents as relDocuments,
  competitions as relCompetitions
 )



class CitizenInsert(admin.TabularInline):
    model=relDocuments.RegionHasCitizenship

    def get_extra(self, request, obj=None, **kwargs):
        if not obj:
            return 1
        elif obj.citizen.count():
            return 0
        else:
            return 1

@admin.register(dictDocuments.Language)
class LanguageAdmin(admin.ModelAdmin):
  pass


@admin.register(dictDocuments.Region)
class RegionAdmin(admin.ModelAdmin):
    inlines = (CitizenInsert,)

@admin.register(dictDocuments.SettlementType)
class SettlementTypeAdmin(admin.ModelAdmin):
  pass


@admin.register(dictDocuments.Citizenship)
class CitizenshipAdmin(admin.ModelAdmin):
  pass


@admin.register(dictDocuments.IdentityDocumentType)
class IdentityDocumentTypeAdmin(admin.ModelAdmin):
  pass


@admin.register(dictDocuments.EducationDocumentType)
class EducationDocumentTypeAdmin(admin.ModelAdmin):
  pass


@admin.register(dictDocuments.IndividualAchievement)
class IndividualAchievementAdmin(admin.ModelAdmin):
  pass


@admin.register(dictDocuments.FinancingType)
class FinancingTypeAdmin(admin.ModelAdmin):
  pass


@admin.register(dictCompetitions.SubjectType)
class SubjectTypeAdmin(admin.ModelAdmin):
  pass


@admin.register(dictCompetitions.Order)
class OrderAdmin(admin.ModelAdmin):
  pass

@admin.register(dictCompetitions.EducationPeriod)
class EducationPeriodAdmin(admin.ModelAdmin):
  pass

@admin.register(dictCompetitions.CompetitionStatus)
class CompetitionStatusAdmin(admin.ModelAdmin):
  pass

@admin.register(dictCompetitions.TargetOrganization)
class TargetOrganizationAdmin(admin.ModelAdmin):
  pass


@admin.register(dictCompetitions.EducationLevelType)
class EducationLevelTypeAdmin(admin.ModelAdmin):
  pass

@admin.register(dictCompetitions.EducationForm)
class EducationFormAdmin(admin.ModelAdmin):
  pass


@admin.register(dictCompetitions.EducationProfileDirection)
class EducationProfileDirectionAdmin(admin.ModelAdmin):
  pass

