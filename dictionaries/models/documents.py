import datetime
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _

class Language(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="Имя"
    )

    class Meta:
        verbose_name=_("Язык")
        verbose_name_plural = "Языки"

    def __str__(self):
        return self.name


class Region(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="Имя"

    )

    class Meta:
        verbose_name=_("Регион")
        verbose_name_plural = "Регионы"

    def __str__(self):
        return self.name


class IdentityDocumentType(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="Имя"
    )

    class Meta:
        verbose_name=_("Тип документа, подтверждающего личность")
        verbose_name_plural = "Типы документов, подтверждающих личность"

    def __str__(self):
        return self.name


class EducationDocumentType(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="Имя"
    )

    class Meta:
        verbose_name=_("Тип документа об образовании")
        verbose_name_plural = "Типы документов об образовании"

    def __str__(self):
        return self.name


class SettlementType(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="Имя"
    )

    class Meta:
        verbose_name=_("Тип населенного пункта")
        verbose_name_plural = "Типы населенных пунктов"

    def __str__(self):
        return self.name


class Citizenship(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="Имя"
    )

    class Meta:
        verbose_name=_("Гражданство")
        verbose_name_plural = "Гражданства"

    def __str__(self):
        return self.name


class IndividualAchievement(models.Model):
    name = models.CharField(max_length=255)
    score = models.IntegerField()

    class Meta:
        verbose_name=_("Индивидуальное достижение")
        verbose_name_plural = "Индивидуальные достижения"

    def __str__(self):
        return self.name


class FinancingType(models.Model):
    name = models.CharField(max_length=255, unique=True)
    short_name = models.CharField(max_length=10)

    class Meta:
        verbose_name=_("Тип финансирования")
        verbose_name_plural = "Типы финансирования"

    def __str__(self):
        return self.name