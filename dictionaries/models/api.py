import datetime
from django.db import models
from django.utils import timezone


class EducationProfileDirection(models.Model):
    code=models.CharField(max_length=12)
    name=models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name

class EducationForm(models.Model):
    name=models.CharField(max_length=255, unique=True)
    short_name=models.CharField(max_length=15)

    def __str__(self):
        return self.name