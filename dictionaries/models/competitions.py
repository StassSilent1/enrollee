import datetime
from django.db import models
from django.utils.translation import gettext as _


class SubjectType(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name=_("Тип предмета")
        verbose_name_plural = "Типы предметов"

    def __str__(self):
        return self.name


class Order(models.Model):
    name = models.CharField(max_length=255, unique=True)
    date = models.DateField()

    class Meta:
        verbose_name=_("Приказ")
        verbose_name_plural = "Приказы"

    def __str__(self):
        return self.name


class EducationPeriod(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name=_("Период обучения")
        verbose_name_plural = "Периоды обучения"

    def __str__(self):
        return self.name


class CompetitionStatus(models.Model):
    name = models.CharField(max_length=255, unique=True)
    color = models.CharField(max_length=7)

    class Meta:
        verbose_name=_("Статус конкурса")
        verbose_name_plural = "Статусы конкурса"

    def __str__(self):
        return self.name


class TargetOrganization(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name=_("Целевая организация")
        verbose_name_plural = "Целевые организации"

    def __str__(self):
        return self.name

class EducationLevelType(models.Model):
    name=models.CharField(max_length=255, unique=True)
    short_name=models.CharField(max_length=15)

    class Meta:
        verbose_name=_("Тип уровня образования")
        verbose_name_plural = "Типы уровня образования"

    def __str__(self):
        return self.name


class EducationForm(models.Model):
    name=models.CharField(max_length=255, unique=True)
    short_name=models.CharField(max_length=15)

    class Meta:
        verbose_name=_("Форма обучения")
        verbose_name_plural = "Формы обучения"

    def __str__(self):
        return self.name

class EducationProfileDirection(models.Model):
    code=models.CharField(max_length=12)
    name=models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name=_("Направление/профиль образования")
        verbose_name_plural = "Направления/профили образования"

    def __str__(self):
        return self.name