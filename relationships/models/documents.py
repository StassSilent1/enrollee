import datetime
from django.utils.translation import gettext as _
from django.db import models
from django.utils import timezone
from dictionaries.models import (
    documents as dictDocuments,
    competitions as dictCompetitions,
)
# from relationships.models import competitions as relCompetitions


class Enrollee(models.Model):
    language = models.ForeignKey(
        dictDocuments.Language,
        on_delete=models.CASCADE,
    )
    email = models.CharField(max_length=255, unique=True)
    book = models.IntegerField()
    created_date = models.DateField()
    updated_date = models.DateField()
    health_description = models.CharField(max_length=511)
    addition_description = models.CharField(max_length=511)
    olympiads = models.CharField(max_length=511)
    hostel = models.SmallIntegerField()
    achievements = models.ManyToManyField(
        dictDocuments.IndividualAchievement,
        blank=True,
    )
    # subjects = models.ManyToManyField(Subject, through=EnroleeHasSubject)

    class Meta:
        verbose_name =_("Абитуриент")
        verbose_name_plural = "Абитуриенты"

    def __str__(self):
        return self.email


class EnrolleePhone(models.Model):
    enrollee = models.ForeignKey(
        Enrollee,
        related_name='phone',
        on_delete=models.CASCADE,
    )
    phone = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.phone


class IdentityDocument(models.Model):
    gender = (
        ('м', 'М'),
        ('ж', 'Ж'),
    )

    enrollee = models.ForeignKey(Enrollee, on_delete=models.CASCADE)
    identity_document_type = models.ForeignKey(
        dictDocuments.IdentityDocumentType,
        related_name='enrollee',
        on_delete=models.CASCADE
    )
    name = models.CharField(max_length=255)
    surname=models.CharField(max_length=255)
    patronymic = models.CharField(max_length=255)
    gender = models.CharField(
        max_length=1,
        choices=gender,
        default='м',
    )
    series = models.CharField(max_length=31, unique=True)
    number = models.CharField(max_length=63, unique=True)
    birthday = models.DateField()
    issue_date = models.DateField()

    class Meta:
        verbose_name =_("Документ подтверждающий личность")
        verbose_name_plural = "Документы подтверждающие личность"

class RegionHasCitizenship(models.Model):
    region = models.ForeignKey(
        dictDocuments.Region,
        on_delete=models.CASCADE,
        related_name='citizen',
        )
    citizenship = models.ForeignKey(
        dictDocuments.Citizenship,
         on_delete=models.CASCADE
         )

    class Meta:
        verbose_name =_("Гражданство региона")
        verbose_name_plural = "Гражданства региона"

    def __str__(self):
        return self.citizenship.email


class PrimaryIdentityDocument(models.Model):
    identity_document = models.OneToOneField(
        IdentityDocument,
        on_delete=models.CASCADE,
        primary_key=True
        )
    region = models.ForeignKey(RegionHasCitizenship, on_delete=models.CASCADE)
    settlement_type = models.ForeignKey(dictDocuments.SettlementType, on_delete=models.CASCADE)
    organization = models.CharField(max_length=511, unique=True)
    birth_place = models.CharField(max_length=511, unique=True)
    address = models.CharField(max_length=511, unique=True)

    class Meta:
        verbose_name =_("Главный документ, подтверждающий личность")
        verbose_name_plural = "Главные документы, подтверждающий личность"



class OtherIdentityDocument(models.Model):
    identity_document = models.OneToOneField(IdentityDocument, on_delete=models.CASCADE, primary_key=True)
    citizenship = models.ForeignKey(dictDocuments.Citizenship, on_delete=models.CASCADE)

    class Meta:
        verbose_name =_("Другой документ, подтверждающий личность")
        verbose_name_plural = "Другие документы, подтверждающиe личность"


class SitizenshipHasIdentityDocument(models.Model):
    citizenship_id = models.ForeignKey(dictDocuments.Citizenship, on_delete=models.CASCADE)
    identity_document_type = models.ForeignKey(dictDocuments.IdentityDocumentType, on_delete=models.CASCADE)


class EducationDocument(models.Model):
    enrollee = models.ForeignKey('Enrollee', on_delete=models.CASCADE)
    education_document_type = models.ForeignKey(dictDocuments.EducationDocumentType, on_delete=models.CASCADE)
    settlement_type = models.ForeignKey(dictDocuments.SettlementType, on_delete=models.CASCADE)
    region = models.ForeignKey(dictDocuments.Region, on_delete=models.CASCADE)
    series = models.CharField(max_length=31, unique=True)
    number = models.CharField(max_length=63, unique=True)
    issue_date = models.DateField()
    education_organization = models.CharField(max_length=511)
    address = models.CharField(max_length=511)

    class Meta:
        verbose_name =_("Документ об образовании")
        verbose_name_plural = "Документы об образовании"

    def __str__(self):
        return self.enrollee.email



class EducationLevelTypeHasDocument(models.Model):
    document_education_type = models.ForeignKey(dictDocuments.EducationDocumentType, on_delete=models.CASCADE)
    edu_level_type = models.ForeignKey(dictCompetitions.EducationLevelType, on_delete=models.CASCADE)
    individual_achievements = models.ManyToManyField(
        dictDocuments.IndividualAchievement,
        blank=True,
    )

    class Meta:
        verbose_name =_("Документ для типа уровня образования")
        verbose_name_plural = "Документы для типа уровня образования"

    def __str__(self):
        return self.edu_level_type.name

class FinancingMethod(models.Model):
    financing_type = models.ForeignKey(dictDocuments.FinancingType, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, unique=True)
    short_name = models.CharField(max_length=10)

    class Meta:
        verbose_name =_("Метод финансирования")
        verbose_name_plural = "Методы финансирования"

    def __str__(self):
        return self.name


class FinancingTypeHasEducationDocument(models.Model):
    financing_type = models.ForeignKey(dictDocuments.FinancingType, on_delete=models.CASCADE)
    education_level_type_has_document_education=models.ForeignKey(EducationLevelTypeHasDocument, on_delete=models.CASCADE)


    class Meta:
        verbose_name =_("Финансирование уровня образования")
        verbose_name_plural = "Финансирование уровней образования"


class EducationPeriodHasFinancingTypeHasDocument(models.Model):
    financing_type_has_education_document = models.ForeignKey(FinancingTypeHasEducationDocument, on_delete=models.CASCADE)
    education_period = models.ForeignKey(dictCompetitions.EducationPeriod, on_delete=models.CASCADE)
    edu_form = models.ForeignKey(dictCompetitions.EducationForm, on_delete=models.CASCADE)

    class Meta:
        verbose_name =_("Финансирование уровня образования для периода")
        verbose_name_plural = "Финансирование уровней образования для периода"
