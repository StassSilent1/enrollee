import datetime
from django.utils.translation import gettext as _
from django.db import models
from django.utils import timezone
from dictionaries.models import(
    competitions as dictCompetitions,
    documents as dictDocuments
    )
from relationships.models import documents as relDocuments


class EducationLevel(models.Model):
    education_level_type = models.ForeignKey(dictCompetitions.EducationLevelType, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name=_("Уровень образования")
        verbose_name_plural = "Уровни образования"

    def __str__(self):
        return self.name


class EducationDirection(models.Model):
    edu_profile_direction = models.OneToOneField(dictCompetitions.EducationProfileDirection, on_delete=models.CASCADE, primary_key=True)
    edu_level = models.ForeignKey(EducationLevel, on_delete=models.CASCADE)

    class Meta:
        verbose_name=_("Направление образования")
        verbose_name_plural = "Направления образования"

    def __str__(self):
        return self.edu_profile_direction.name


class EducationProfile(models.Model):
    edu_profile_direction = models.OneToOneField(dictCompetitions.EducationProfileDirection, on_delete=models.CASCADE, primary_key=True)
    edu_direction = models.ForeignKey(EducationDirection, on_delete=models.CASCADE)

    class Meta:
        verbose_name=_("Профиль образования")
        verbose_name_plural = "Профили образования"

    def __str__(self):
        return self.edu_profile_direction.name


class CountCompliance(models.Model):
    edu_form = models.OneToOneField(dictCompetitions.EducationForm, on_delete=models.CASCADE)
    enrollee = models.ForeignKey(relDocuments.Enrollee, on_delete=models.CASCADE)
    count = models.IntegerField()


class Subject(models.Model):
    subject_type = models.ForeignKey(dictCompetitions.SubjectType, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=31)
    min_score = models.IntegerField()



class Competition(models.Model):
    edu_profile_direction = models.ForeignKey(dictCompetitions.EducationProfileDirection, on_delete=models.CASCADE)
    edu_form = models.ForeignKey(dictCompetitions.EducationForm, on_delete=models.CASCADE)
    financing_method = models.ForeignKey(relDocuments.FinancingMethod, on_delete=models.CASCADE)
    number_seats = models.IntegerField()
    is_close = models.BooleanField()

    class Meta:
        verbose_name=_("Конкурс")
        verbose_name_plural = "Конкурсы"

    def __str__(self):
        return self.edu_profile_direction.name


class CompetitionHasSubject(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    priority = models.IntegerField()


class CompetitionProfile(models.Model):
    edu_profile = models.ForeignKey(EducationProfile, on_delete=models.CASCADE)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)

    class Meta:
        verbose_name =_("Конкурс профиля")
        verbose_name_plural = "Конкурсы профиля"

    def __str__(self):
        return self.competition.edu_profile_direction.name


class EnroleeHasSubject(models.Model):
    class Meta:
        unique_together=(('enrollee','subject'),)

    enrollee = models.ForeignKey(relDocuments.Enrollee, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    is_ege=models.BooleanField()
    is_verificated=models.BooleanField()

    class Meta:
        verbose_name =_("Предмет абитуриента")
        verbose_name_plural = "Предметы абитуриента"

    def __str__(self):
        return self.enrollee.email


class EnroleeHasCompetition(models.Model):
    enrollee = models.ForeignKey(
        relDocuments.Enrollee,
        on_delete=models.CASCADE,
        related_name='competition',
        )
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    edu_profile = models.ForeignKey(EducationProfile, on_delete=models.CASCADE)
    edu_period = models.ForeignKey(dictCompetitions.EducationPeriod, on_delete=models.CASCADE)
    competition_status = models.ForeignKey(dictCompetitions.CompetitionStatus, on_delete=models.CASCADE)
    order = models.ForeignKey(dictCompetitions.Order, on_delete=models.CASCADE)
    target_organisation = models.ForeignKey(
        dictCompetitions.TargetOrganization,
        on_delete=models.CASCADE,
        blank=True,
        null=True)
    consent = models.BooleanField()
    date_consent = models.DateTimeField()
    payment = models.DecimalField(decimal_places=2, max_digits=2)
    date_payment = models.DateTimeField()

    class Meta:
        verbose_name =_("Конкурс абитуриента")
        verbose_name_plural = "Конкурсы абитуриента"

    def __str__(self):
        return self.competition.edu_profile_direction.name

