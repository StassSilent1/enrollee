from django.contrib import admin
from relationships.models import(
    documents as relDocuments,
    competitions as relCompetitions
    )
from dictionaries.models import(
    documents as dictDocuments,
    competitions as dictCompetitions
    )



class PhoneInline(admin.StackedInline):
    model = relDocuments.EnrolleePhone

    def get_extra(self, request, obj=None, **kwargs):
        if not obj:
            return 1
        elif obj.phone.count():
            return 0
        else:
            return 1




class DocInline(admin.StackedInline):
    model = relDocuments.IdentityDocument
    extra = 1
    radio_fields = {"gender": admin.HORIZONTAL}
    def get_extra(self, request, obj=None, **kwargs):
        if not obj:
            return 1
        elif obj.phone.count():
            return 0
        else:
            return 1


class EnroleeHasCompetitionInline(admin.TabularInline):
    model = relCompetitions.EnroleeHasCompetition
    extra = 1

    def get_extra(self, request, obj=None, **kwargs):
        if not obj:
            return 1
        elif obj.competition.count():
            return 0
        else:
            return 1


class PrimaryDocInline(admin.StackedInline):
    model = relDocuments.PrimaryIdentityDocument

    def get_max_num(self, request, obj=None, **kwargs):
        max_num = 1
        return max_num



class CompetitionProfileInline(admin.StackedInline):
    model = relCompetitions.CompetitionProfile
    extra = 1


class EducationPeriodHasFinancingTypeHasDocumentInline(admin.StackedInline):
    model = relDocuments.EducationPeriodHasFinancingTypeHasDocument
    extra = 1


# class OtherDocInline(admin.StackedInline):
#     model = relDocuments.OtherIdentityDocument
#     extra = 1


class FinancingTypeHasEducationDocumentInline(admin.StackedInline):
    model = relDocuments.FinancingTypeHasEducationDocument
    extra = 1


@admin.register(relDocuments.Enrollee)
class EnrolleeAdmin(admin.ModelAdmin):
    inlines = [
    PhoneInline,
    EnroleeHasCompetitionInline,
    DocInline,
    ]
    filter_horizontal = ('achievements',)


@admin.register(relDocuments.IdentityDocument)
class IdentityDocumentAdmin(admin.ModelAdmin):
    list_display = ('enrollee','name', 'surname','patronymic')


@admin.register(relDocuments.EducationDocument)
class EducationDocumentAdmin(admin.ModelAdmin):
    pass


@admin.register(relDocuments.OtherIdentityDocument)
class OtherIdentityDocumentAdmin(admin.ModelAdmin):
    pass


@admin.register(relCompetitions.EducationLevel)
class EducationLevelAdmin(admin.ModelAdmin):
    pass


@admin.register(relCompetitions.EducationDirection)
class EducationDirectionAdmin(admin.ModelAdmin):
    pass


@admin.register(relCompetitions.EducationProfile)
class EducationProfileAdmin(admin.ModelAdmin):
    inlines = [
    CompetitionProfileInline,
    ]



@admin.register(relCompetitions.Competition)
class CompetitionAdmin(admin.ModelAdmin):
    pass


@admin.register(relDocuments.FinancingMethod)
class FinancingMethodAdmin(admin.ModelAdmin):
    pass

@admin.register(relDocuments.EducationLevelTypeHasDocument)
class EducationLevelTypeHasDocumentAdmin(admin.ModelAdmin):
    filter_horizontal = ('individual_achievements',)
    inlines = [
        FinancingTypeHasEducationDocumentInline,
    ]

@admin.register(relDocuments.FinancingTypeHasEducationDocument)
class FinancingTypeHasEducationDocumentAdmin(admin.ModelAdmin):
   list_display = ('financing_type','education_level_type_has_document_education')
   inlines = [
        EducationPeriodHasFinancingTypeHasDocumentInline,
   ]