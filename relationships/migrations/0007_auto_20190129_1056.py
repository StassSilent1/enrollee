# Generated by Django 2.1.5 on 2019-01-29 07:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relationships', '0006_auto_20190129_1049'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='educationdirection',
            options={'verbose_name': 'Направление образования', 'verbose_name_plural': 'Направления образования'},
        ),
        migrations.RenameField(
            model_name='educationdirection',
            old_name='edu_level_id',
            new_name='edu_level',
        ),
    ]
