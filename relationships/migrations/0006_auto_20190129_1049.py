# Generated by Django 2.1.5 on 2019-01-29 07:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relationships', '0005_auto_20190129_1046'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='educationlevel',
            options={'verbose_name': 'Уровень образования', 'verbose_name_plural': 'Уровни образования'},
        ),
    ]
