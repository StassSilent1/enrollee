# Generated by Django 2.1.5 on 2019-01-30 08:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relationships', '0014_auto_20190130_1139'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='otheridentitydocument',
            options={'verbose_name': 'Другой документ, подтверждающий личность', 'verbose_name_plural': 'Другие документы, подтверждающиe личность'},
        ),
    ]
