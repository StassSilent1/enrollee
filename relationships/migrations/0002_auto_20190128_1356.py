# Generated by Django 2.1.5 on 2019-01-28 10:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0002_auto_20190128_1356'),
        ('relationships', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='EducationDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('series', models.CharField(max_length=31, unique=True)),
                ('number', models.CharField(max_length=63, unique=True)),
                ('issue_date', models.DateField()),
                ('education_organization', models.CharField(max_length=511)),
                ('address', models.CharField(max_length=511)),
                ('education_document_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.EducationDocumentType')),
            ],
        ),
        migrations.CreateModel(
            name='EducationLevelTypeHasDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('document_education_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.EducationDocumentType')),
                ('edu_level_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.EducationLevelType')),
                ('individual_achievements', models.ManyToManyField(to='dictionaries.IndividualAchievement')),
            ],
        ),
        migrations.CreateModel(
            name='EducationPeriodHasFinancingTypeHasDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('edu_form', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.EducationForm')),
                ('education_period', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.EducationPeriod')),
            ],
        ),
        migrations.CreateModel(
            name='FinancingMethod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('short_name', models.CharField(max_length=10)),
                ('financing_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.FinancingType')),
            ],
        ),
        migrations.CreateModel(
            name='FinancingTypeHasEducationDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('education_level_type_has_document_education', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.EducationLevelType')),
                ('financing_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='relationships.EducationLevelTypeHasDocument')),
            ],
        ),
        migrations.CreateModel(
            name='IdentityDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('surname', models.CharField(max_length=255)),
                ('patronymic', models.CharField(max_length=255)),
                ('gender', models.CharField(choices=[('м', 'М'), ('ж', 'Ж')], default='м', max_length=1)),
                ('series', models.CharField(max_length=31, unique=True)),
                ('number', models.CharField(max_length=63, unique=True)),
                ('birthday', models.DateField()),
                ('issue_date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='RegionHasCitizenship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('citizenship', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.Citizenship')),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.Region')),
            ],
        ),
        migrations.CreateModel(
            name='SitizenshipHasIdentityDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('citizenship_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.Citizenship')),
                ('identity_document_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.IdentityDocumentType')),
            ],
        ),
        migrations.RenameField(
            model_name='enrolleephone',
            old_name='enrollee_id',
            new_name='enrollee',
        ),
        migrations.AlterField(
            model_name='enrollee',
            name='language',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.Language'),
        ),
        migrations.CreateModel(
            name='OtherIdentityDocument',
            fields=[
                ('identity_document', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='relationships.IdentityDocument')),
                ('citizenship', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.Citizenship')),
            ],
        ),
        migrations.CreateModel(
            name='PrimaryIdentityDocument',
            fields=[
                ('identity_document', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='relationships.IdentityDocument')),
                ('organization', models.CharField(max_length=511, unique=True)),
                ('birth_place', models.CharField(max_length=511, unique=True)),
                ('address', models.CharField(max_length=511, unique=True)),
                ('region_has_citizenship', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='relationships.RegionHasCitizenship')),
                ('settlement_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.SettlementType')),
            ],
        ),
        migrations.AddField(
            model_name='identitydocument',
            name='enrollee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='relationships.Enrollee'),
        ),
        migrations.AddField(
            model_name='identitydocument',
            name='identity_document_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.IdentityDocumentType'),
        ),
        migrations.AddField(
            model_name='educationperiodhasfinancingtypehasdocument',
            name='financing_type_has_education_document',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='relationships.FinancingTypeHasEducationDocument'),
        ),
        migrations.AddField(
            model_name='educationdocument',
            name='enrollee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='relationships.Enrollee'),
        ),
        migrations.AddField(
            model_name='educationdocument',
            name='region',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.Region'),
        ),
        migrations.AddField(
            model_name='educationdocument',
            name='settlement_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaries.SettlementType'),
        ),
    ]
